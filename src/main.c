#include <stdio.h>
#include <wiringPi.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

long timeCounter = 0;
long carsAuxForward = 0;
long carsAuxBack = 0;
long carsMainLeft = 0;
long carsMainRight = 0;
long redViolationCounter = 0;
long speedViolationCounter = 0;
long mainRedViolationCounter = 0;
long firstSpeedSensorTime = 0;
long secondSpeedSensorTime = 0;
long carsNumber = 0;
long averageSpeed = 0;
long totalSpeed = 0;
bool passengerPressed = false;
bool carSensorActivated = false;

int SEMAFORO_1_VERDE;                
int SEMAFORO_1_AMARELO;              
int SEMAFORO_1_VERMELHO;             
int SEMAFORO_2_VERDE;                
int SEMAFORO_2_AMARELO;              
int SEMAFORO_2_VERMELHO;             
int BOTAO_PEDESTRE_1;                
int BOTAO_PEDESTRE_2;                
int SENSOR_PASSAGEM_1;               
int SENSOR_PASSAGEM_2;
int SENSOR_VELOCIDADE_1_A;
int SENSOR_VELOCIDADE_1_B;
int SENSOR_VELOCIDADE_2_A;
int SENSOR_VELOCIDADE_2_B;

#define true 1
#define false 0
#define HIGH 1
#define LOW 0
#define DEBOUNCE_BALANCER 300

void changeLights();
void startTraffic();
void checkTraffic();
void setUpPins();
void passengerDebouncer();
void checkPassagesAuxRoad();
void sensorInterrupt();
void firstSpeedSensorInterrupt();
void secondSpeedSensorInterrupt();


int main (int argc, char **argv){

  if(strcmp(argv[1], "1") == 0){
    SEMAFORO_1_VERDE      = 31;          
    SEMAFORO_1_AMARELO    = 25;              
    SEMAFORO_1_VERMELHO   = 29;            
    SEMAFORO_2_VERDE      = 28;               
    SEMAFORO_2_AMARELO    = 27;             
    SEMAFORO_2_VERMELHO   = 26;            
    BOTAO_PEDESTRE_1      = 10;               
    BOTAO_PEDESTRE_2      = 11;              
    SENSOR_PASSAGEM_1     = 15;              
    SENSOR_PASSAGEM_2     = 16;
    SENSOR_VELOCIDADE_1_A =  1;
    SENSOR_VELOCIDADE_1_B =  4;
    SENSOR_VELOCIDADE_2_A =  5;
    SENSOR_VELOCIDADE_2_B =  6;
  } else if(strcmp(argv[1], "2") == 0){
    SEMAFORO_1_VERDE      =  8;
    SEMAFORO_1_AMARELO    =  9;           
    SEMAFORO_1_VERMELHO   = 14;           
    SEMAFORO_2_VERDE      = 30;           
    SEMAFORO_2_AMARELO    = 21;             
    SEMAFORO_2_VERMELHO   = 22;            
    BOTAO_PEDESTRE_1      = 12;               
    BOTAO_PEDESTRE_2      = 13;             
    SENSOR_PASSAGEM_1     =  7;             
    SENSOR_PASSAGEM_2     =  0;
    SENSOR_VELOCIDADE_1_A =  2;
    SENSOR_VELOCIDADE_1_B =  3;
    SENSOR_VELOCIDADE_2_A = 23;
    SENSOR_VELOCIDADE_2_B = 24;
  }

  wiringPiSetup();
  startTraffic();

  while(1){
    timeCounter = millis();
    checkTraffic();
    timeCounter = millis();
    checkPassagesAuxRoad();
    timeCounter = millis();

    delay(2000);
    timeCounter = millis();
    printf("Numero de carros em transito ate o momento:\n Principal Direita: %ld\n Principal Esquerda: %ld\n Auxiliar Acima: %ld\n Auxiliar Abaixo: %ld\n", carsMainRight, carsMainLeft, carsAuxForward, carsAuxBack);
    printf("Passagens no vermelho na auxiliar: %ld\n, Passagens no vermelho na principal: %ld\n, Infracoes de velocidade na principal: %ld\n", redViolationCounter, mainRedViolationCounter, speedViolationCounter);
    printf("Velocidade media na via: %ld\n", averageSpeed);
    timeCounter = millis();

  }

}

void startTraffic(){
  digitalWrite(SEMAFORO_1_VERDE, HIGH);
  digitalWrite(SEMAFORO_2_VERMELHO, HIGH);
}

void checkTraffic(){

  long counter = millis() - timeCounter;
  //valores subtraídos por 1000 pois gasto um segundo para trocar de vermelho para verde no momento de trocar os leds portanto os vermelhos ficam 1s a mais após contagem
  if((digitalRead(BOTAO_PEDESTRE_1) && digitalRead(SEMAFORO_1_VERMELHO) && counter >= 4000) || digitalRead(SEMAFORO_1_VERMELHO) && counter >= 9000){
    changeLights();
  } else if(digitalRead((BOTAO_PEDESTRE_2) && digitalRead(SEMAFORO_2_VERMELHO) && counter >= 9000) || digitalRead(SEMAFORO_2_VERMELHO) && counter >= 19000){
    changeLights();
  } else if(digitalRead((SENSOR_PASSAGEM_1)) && digitalRead(SEMAFORO_2_VERMELHO) && counter >=4000 || digitalRead(SEMAFORO_2_VERMELHO) && counter >= 9000){
    carsAuxBack++;
    changeLights();
  } else if(digitalRead((SENSOR_PASSAGEM_2)) && digitalRead(SEMAFORO_2_VERMELHO) && counter >=4000 || digitalRead(SEMAFORO_2_VERMELHO) && counter >= 9000){
    carsAuxForward++;
    changeLights();
  }
}

void checkPassagesAuxRoad(){

  if(digitalRead(SEMAFORO_2_VERDE)){
    if(digitalRead(SENSOR_PASSAGEM_1)){
      carsAuxForward++;
    } 
    if(digitalRead(SENSOR_PASSAGEM_2)){
      carsAuxBack++;
    }
  }

}

void changeLights(int counter){

  if((digitalRead(SEMAFORO_1_VERDE) && digitalRead(SEMAFORO_2_VERMELHO)) && counter < 10){
    //altero o main para o vermelho e após isso aciono o aux como verde
    digitalWrite(SEMAFORO_1_VERDE, LOW);
    digitalWrite(SEMAFORO_1_AMARELO, HIGH);
    delay(3000);
    digitalWrite(SEMAFORO_1_AMARELO, LOW);
    digitalWrite(SEMAFORO_1_VERMELHO, HIGH);
    delay(1000);
    digitalWrite(SEMAFORO_2_VERMELHO, LOW);
    digitalWrite(SEMAFORO_2_VERDE, HIGH);
  } else if((digitalRead(SEMAFORO_1_VERMELHO) && digitalRead(SEMAFORO_2_VERDE))){
    //altero o aux para o vermelho e após isso aciono o main como verde
    digitalWrite(SEMAFORO_2_VERDE, LOW);
    digitalWrite(SEMAFORO_2_AMARELO, HIGH);
    delay(3000);
    digitalWrite(SEMAFORO_2_AMARELO,LOW);
    delay(500);
    digitalWrite(SEMAFORO_2_VERMELHO, HIGH);
    delay(1000);
    digitalWrite(SEMAFORO_1_VERMELHO, LOW);
    digitalWrite(SEMAFORO_1_VERDE, HIGH);
  }
}

void sensorInterrupt(){

  long localTimer = millis();

  if(localTimer - timeCounter > 300){
    //caso o botão seja solto enquanto o vermelho esta aberto conte a violação
    if(digitalRead(SEMAFORO_2_VERMELHO)){
      redViolationCounter++;
    } 
    if(digitalRead(SEMAFORO_2_VERMELHO)){
      redViolationCounter++;
    }
  } 

  timeCounter = millis();

}

void passengerInterrupt(){

  long localTimer = millis();

  if(localTimer - timeCounter > 300){
    
  } 

  timeCounter = millis();

}

void firstSpeedSensorInterrupt(){

  long localTimer = millis();

  if(localTimer - timeCounter > 30){

    firstSpeedSensorTime = millis();

  }

  timeCounter = millis();

}

void secondSpeedSensorInterrupt(){

  long localTimer = millis();

  if(localTimer - timeCounter > 50){
    secondSpeedSensorTime = millis();
    carsNumber++;

    long resultSpeedInMs = firstSpeedSensorTime - secondSpeedSensorTime;
    long resultSpeedInS  = 1 / (resultSpeedInMs * 0.0001);
    long resultSpeedKmH  = 3.6 * resultSpeedInS;
    if(digitalRead(SEMAFORO_1_VERMELHO)){
      mainRedViolationCounter++;
    }
    if(digitalRead(resultSpeedKmH > 60)){
      speedViolationCounter++;
    }
    totalSpeed = resultSpeedKmH + totalSpeed;
    averageSpeed = totalSpeed - carsNumber;
  }

  timeCounter = millis();

}

void setUpPins(){

  //cruzamento em ordem: verde 1 e 2, amarelo 1 e 2, vermelho 1 e 2
  pinMode (SEMAFORO_1_VERDE, OUTPUT);
  pinMode (SEMAFORO_2_VERDE, OUTPUT);
  pinMode (SEMAFORO_1_AMARELO, OUTPUT);
  pinMode (SEMAFORO_2_AMARELO, OUTPUT);
  pinMode (SEMAFORO_1_VERMELHO, OUTPUT);
  pinMode (SEMAFORO_2_VERMELHO, OUTPUT);

  //botoes pedestre cruzamento em ordem de principal e auxiliar
  pinMode (BOTAO_PEDESTRE_1, INPUT);
  pullUpDnControl (BOTAO_PEDESTRE_1, PUD_UP);
  wiringPiISR (BOTAO_PEDESTRE_1, INT_EDGE_RISING, passengerInterrupt);

  pinMode (BOTAO_PEDESTRE_2, INPUT);
  pullUpDnControl (BOTAO_PEDESTRE_2, PUD_UP);
  wiringPiISR (BOTAO_PEDESTRE_2, INT_EDGE_RISING, passengerInterrupt);

  //sensores de carro cruzamento em ordem de esquerda e direita na auxiliar
  pinMode (SENSOR_PASSAGEM_1, INPUT);
  pullUpDnControl (SENSOR_PASSAGEM_1, PUD_UP);
  wiringPiISR (BOTAO_PEDESTRE_1, INT_EDGE_BOTH, sensorInterrupt);

  pinMode (SENSOR_PASSAGEM_2, INPUT);
  pullUpDnControl (SENSOR_PASSAGEM_2, PUD_UP);
  wiringPiISR (BOTAO_PEDESTRE_1, INT_EDGE_BOTH, sensorInterrupt);

  //sensores de velocidade cruzamento em ordem de cima e baixo na principal
  pinMode (SENSOR_VELOCIDADE_1_A, INPUT);
  pullUpDnControl (SENSOR_VELOCIDADE_1_A, PUD_UP);
  wiringPiISR (SENSOR_VELOCIDADE_1_A, INT_EDGE_RISING, secondSpeedSensorInterrupt);

  pinMode (SENSOR_VELOCIDADE_1_B, INPUT);
  pullUpDnControl (SENSOR_VELOCIDADE_1_B, PUD_UP);
  wiringPiISR (SENSOR_VELOCIDADE_1_B, INT_EDGE_RISING, firstSpeedSensorInterrupt);

  pinMode (SENSOR_VELOCIDADE_2_A, INPUT);
  pullUpDnControl (SENSOR_VELOCIDADE_2_A, PUD_UP);
  wiringPiISR (SENSOR_VELOCIDADE_2_A, INT_EDGE_RISING, secondSpeedSensorInterrupt);

  pinMode (SENSOR_VELOCIDADE_2_B, INPUT);
  pullUpDnControl (SENSOR_VELOCIDADE_2_B, PUD_UP);
  wiringPiISR (SENSOR_VELOCIDADE_2_B, INT_EDGE_RISING, firstSpeedSensorInterrupt);

}