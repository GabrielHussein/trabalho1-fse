# Trabalho1-fse

Trabalho 1 de FSE do aluno Gabriel Alves Hussein
Matrícula: 17/0103200

## Compilando o trabalho

Para compilar é necessário apenas estar na raíz do projeto e rodar o comando `make` no terminal

## Rodando o trabalho

Na rasp após compilar o trabalho para rodar é necessário ir na pasta /bin e executar o comando `./bin 1` e em outra instância do terminal o comando `./bin 2` para que sejam executados todos os cruzamentos com os pinos configurados.