# Compiler and flags:
CC = gcc
LDFLAGS = -lwiringPi

BLDDIR = .

# Directory of source files:
SOURCEDIR = $(BLDDIR)/src

# Directory of headers:
INCLUDEDIR = $(BLDDIR)/include

# Directory of objects:
OBJDIR = $(BLDDIR)/obj

CFLAGS = -c -w -I$(INCLUDEDIR)
SOURCE = $(wildcard $(SOURCEDIR)/*.c)
OBJ= $(patsubst $(SOURCEDIR)/%.c, $(OBJDIR)%.o, $(SRC))

EXE = bin/bin	

all: clean $(EXE)

$(EXE): $(OBJ)
	$(CC) $(SOURCEDIR)/*.c -o $@ $(LDFLAGS)

$(OBJDIR)/%.o: $(SOURCEDIR)/%.c
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) $< -o $@

clean:
	-rm -f $(OBJDIR)/*.o $(EXE)